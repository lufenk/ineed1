# Installation
Reference: Follow ionic tutorial: http://ionicframework.com/docs/guide/installation.html

1. Install node v4
https://nodejs.org/en/
2. Install cordova and ionic
sudo npm install -g cordova
sudo npm install -g ionic
3. Install gulp
sudo npm install -g gulp
4. Rest of node modules
sudo npm install
5. Clone the project
git clone ...
6. Restore state
ionic state restore

## Paths to build android app

`export ANDROID_HOME=/Applications/ADT/sdk`
`export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools`

Set them forever: http://blog.just2us.com/2011/05/setting-path-variable-in-mac-permanently/