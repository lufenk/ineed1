// Ionic App
angular.module('app', [
  'ionic','ionic.service.core', 
  'app.controllers', 
  'app.routes', 
  'app.services', 
  'app.filters', 
  'app.directives',
  'angularMoment',
  'monospaced.elastic',
  'satellizer',
  'angular-loading-bar',
  'ngStorage',
  'ion-google-place',
  'ion-autocomplete',
  'pusher-angular'
]);