angular.module('app.controllers', [])
// App Controller
.controller('appCtrl', function($scope, $localStorage, inConfig, Auth, $pusher){

  // Me user object
  $scope.me = $localStorage.me;

  // Setup event listener for pusher on user level
  if(Auth.isAuthenticated()) {

      // Do Pusher Setup
      window.client = new Pusher(inConfig.PUSHER_API_KEY, {
          authEndpoint: inConfig.API_URL + "auth/pusher",
          cluster: "eu",
          auth: {
              headers: {
                  'Authorization': "Bearer " + Auth.getToken()
              }
          }
      });

      var pusher = $pusher(client);
      var privateChannel = pusher.subscribe('user-' + $scope.me.id);
          privateChannel.bind('NewMessage',
          function(data) {
              console.log(data);
          }
      );
  }

})

// Query Controller
.controller('queryCtrl', function($scope, DataService, $filter, LocationService, $state, Util) {

  // Query for conversation
  $scope.q = {
    provider_count : 5,
    current_location : $scope.me.current_location || false
  };

  // Get list of services
  var services  = DataService.getBootData().services;

  $scope.getServices = function (query) {
      if (query) {
          var filteredItems = $filter('filter')(services, { name: query });
          return { items: filteredItems };
      }
      return {items: []};
  };

  $scope.getLocation = function (argument) {
    if($scope.q.current_location) {
      $scope.q.current_location = false;
      // Get location
      LocationService.getCurrentLocation().then(function (pos) {
        $scope.q.current_location = true;
        // assign coords
        $scope.q.lat = pos.latitude;
        $scope.q.lon = pos.longitude;
      });  
    }
  };

  // Start conversation 
  $scope.startConversation = function (e) {
    e.preventDefault();

    DataService.post('conversations', $scope.q).then(function (res) {
      // go to newly created conversation
      $state.go('app.conversation', { id : res.data.conversation.id });
    }).catch(function (err) {
      Util.showValidationError(err);
    });
  };
  
})

// Login Controller
.controller('loginCtrl', function($scope, Auth, $ionicPopup, $state, Util) {
  // User
  $scope.user = {};
  $scope.geo = {};

  // SignUp a user
  $scope.signUp = function (e) {
    e.preventDefault();

    // Split first Name and Last Name 
    var name = Util.splitName($scope.user.name);

    $scope.user.first_name = name.firstName;
    $scope.user.last_name = name.lastName;

    // check for address

    Auth.signup($scope.user).then(function (res) {
      // Login User
      Auth.setToken(res);

      // Setup profile
      Auth.setupProfile(res);

      // redirect user
      $state.go('app.query');

    }).catch(function (res) {
      // show the error
      Util.showValidationError(res);
    });
  };

  // Login using Email 
  $scope.login = function (e) {
    e.preventDefault();
    
    Auth.login($scope.user).then(function (res) {
      // redirect user
      $state.go('app.query');
    }, function (res) {
      // show the error
      Util.showValidationError(res);
    });

  };

  // Social Auth 
  $scope.authenticate = function(provider) {
      Auth.authenticate(provider)
        .then(function() {
          $ionicPopup.alert({
            title: 'Success',
            content: 'You have successfully logged in!'
          });
        })
        .catch(function(response) {
          $ionicPopup.alert({
            title: 'Error',
            content: response.data ? response.data || response.data.message : response
          });
        });
    };


    $scope.logout = function() {
      Auth.logout();
    };

    $scope.isAuthenticated = function() {
      return Auth.isAuthenticated();
    };
})

// Conversation Controller
.controller('conversationCtrl', function($scope, Util, $ionicActionSheet, $timeout, $state, $ionicScrollDelegate, ChatService, conversationData, $stateParams, $pusher, $interval, $ionicModal, inConfig) {
    // Conversation
    $scope.con  = conversationData.data.conversation;

    // Current user
    $scope.user = $scope.me;

    // Pusher
    var pusher = $pusher(client);

    $scope.input = {
      message: localStorage['userMessage-' + $scope.me.id] || ''
    };

    var messageCheckTimer;

    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
    var footerBar; // gets set in $ionicView.enter
    var scroller;
    var txtInput; // ^^^

    $scope.$on('$ionicView.enter', function() {
      console.log('UserMessages $ionicView.enter');

      getMessages();
      
      $timeout(function() {
        footerBar = document.body.querySelector('#conversation .bar-footer');
        scroller = document.body.querySelector('#conversation .scroll-content');
        txtInput = angular.element(footerBar.querySelector('textarea'));
      }, 0);

      // Subscribe it
      var channel = pusher.subscribe('con-' + $scope.con.id);
          channel.bind('NewMessage',
            function(data) {
              if(data.user_id !== $scope.user.id) {
                $scope.con.messages.push(data);
              }

              // scroll it
              $timeout(function() {
                keepKeyboardOpen();
                viewScroll.scrollBottom(true);
              }, 0);
            }
          );

          // vendor leave 
          channel.bind('VendorLeaved', function (data) {
              $scope.con.messages.push(angular.extend(data, { created_at : new Date() }));
              $timeout(function() {
                viewScroll.scrollBottom();
              }, 0);
          });

          // subscribe for NewBid
          var channelMe = pusher.subscribe('user-' + $scope.user.id);
            channelMe.bind('NewBid', function (data) {
              // loop and update the bids for this
              $scope.con.bids.push(data);

              $scope.con.messages.push({
                text : 'New proposal from ' + data.user.first_name,
                created_at : new Date(),
                type : 'log'
              });

              $timeout(function() {
                viewScroll.scrollBottom();
              }, 0);
            });

            channelMe.bind('BidUpdate', function (data) {
              // loop and update the bids for this
              $scope.con.messages.push({
                text : 'Updated proposal from ' + data.user.first_name,
                created_at : new Date(),
                type : 'log'
              });

              $timeout(function() {
                viewScroll.scrollBottom();
              }, 0);

              angular.forEach($scope.con.bids, function (bid, index) {
                if(bid.id === data.id) {
                  $scope.con.bids[index] = data;
                }
              });
            });

      messageCheckTimer = $interval(function() {
        // here you could check for new messages if your app doesn't use push notifications or user disabled them
      }, 20000);
    });

    $scope.$on('$ionicView.leave', function() {
      //console.log('leaving UserMessages view, destroying interval');
      // Make sure that the interval is destroyed
      if (angular.isDefined(messageCheckTimer)) {
        $interval.cancel(messageCheckTimer);
        messageCheckTimer = undefined;
      }

      // unsubscribe from conversation
      pusher.unsubscribe('con-' + $scope.con.id);
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      if (!$scope.input.message || $scope.input.message === '') {
        localStorage.removeItem('userMessage-' + $scope.me.id);
      }
    });

    function getMessages() {
      $scope.messages = $scope.con.messages;
      // the service is mock but you would probably pass the toUser's GUID here
      $timeout(function() {
          viewScroll.scrollBottom();
        }, 0);
    }

    $scope.$watch('input.message', function(newValue, oldValue) {
      if (!newValue) newValue = '';
      localStorage['userMessage-' + $scope.me.id] = newValue;
    });

    $scope.sendMessage = function(sendMessageForm) {

      var message = {
        text: $scope.input.message,
        conversation_id : $stateParams.id,
        user : $scope.user,
        user_id : $scope.user.id,
        sent : false,
        created_at : new Date()
      };

      // if you do a web service call this will be needed as well as before the viewScroll calls
      // you can't see the effect of this in the browser it needs to be used on a real device
      // for some reason the one time blur event is not firing in the browser but does on devices
      keepKeyboardOpen();
      
      $scope.messages.push(message);

      // empty the message to avoide dupes
      $scope.input.message = '';

      $timeout(function() {
        keepKeyboardOpen();
        viewScroll.scrollBottom(true);
      }, 0);

      ChatService.send(message).then(function(res) {
        // Mark sent to true
        message.sent = true;
        message.id = res.data.message.id;
      });

    };
    
    // this keeps the keyboard open on a device only after sending a message, it is non obtrusive
    function keepKeyboardOpen() {
      console.log('keepKeyboardOpen');
      txtInput.one('blur', function() {
        console.log('textarea blur, focus back on it');
        txtInput[0].focus();
      });
    }

    $scope.onMessageHold = function(e, itemIndex, message) {
      console.log('onMessageHold');
      console.log('message: ' + JSON.stringify(message, null, 2));
      $ionicActionSheet.show({
        buttons: [{
          text: 'Copy Text'
        }, {
          text: 'Delete Message'
        }],
        buttonClicked: function(index) {
          switch (index) {
            case 0: // Copy Text
              //cordova.plugins.clipboard.copy(message.text);

              break;
            case 1: // Delete
              // no server side secrets here :~)
              $scope.messages.splice(itemIndex, 1);
              $timeout(function() {
                viewScroll.resize();
              }, 0);

              break;
          }
          
          return true;
        }
      });
    };

    // this prob seems weird here but I have reasons for this in my app, secret!
    $scope.viewProfile = function(msg) {
      if (msg.userId === $scope.user._id) {
        // go to your profile
      } else {
        // go to other users profile
      }
    };

    function onProfilePicError(ele) {
      this.ele.src = ''; // set a fallback
    }
    
    // I emit this event from the monospaced.elastic directive, read line 480
    $scope.$on('0ff-elastic:resize', function(e, ta) {
      console.log('elastic:resize');
      if (!ta) return;
      
      var taHeight = ta[0].offsetHeight;
      console.log('taHeight: ' + taHeight);
      
      if (!footerBar) return;
      
      var newFooterHeight = taHeight + 16;
      newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
      
      footerBar.style.height = newFooterHeight + 'px';
      scroller.style.bottom = newFooterHeight + 'px'; 

      // resize the scroll area
      // viewScroll.resize();
    });

    // Conversation details 
    $ionicModal.fromTemplateUrl('templates/modal/conversation-details.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Proposal details 
    $ionicModal.fromTemplateUrl('templates/modal/proposal-details.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.proposalModal = modal;
    });

    $scope.showDetails = function() {
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.proposal = function() {
      $scope.proposalModal.show();
    };

    $scope.closeProposalModal = function() {
      $scope.proposalModal.hide();
    };    

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
      $scope.proposalModal.remove();
    });

    $scope.leave = function () {
      Util.confirm({
          title : 'Want to exit?',
          template : 'Are you sure want to exit, this thread will be archived an no longer you can recieve reply on this?'
      }, function(answer){
        if(answer) {
          ChatService.archiveConversation($scope.con.id).then(function (res) {
            $state.go('app.chats', {}, { reload : true});
          });  
        }
      });
    };

    // Payment model 
    $ionicModal.fromTemplateUrl('templates/modal/pay.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.payModal = modal;
    });

    $scope.closePayModal = function() {
      $scope.payModal.hide();
    };

    $scope.approveBid = function (bid) {
      $scope.bid = bid;
      $scope.payModal.show();
      //$state.go('pay', { id : bid.id, amount : bid.total });
    };

    var handler = StripeCheckout.configure({
      key: inConfig.STRIPE_KEY,
      //image: '/img/documentation/checkout/marketplace.png',
      locale: 'auto',
      token: function(token) {
        console.log(token);
        Util.popup('Payment Successfull', 'Your payment was Successfull');
        $state.go('app.query');
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    $scope.payButton = function (e) {
      // Open Checkout with further options:
      handler.open({
        name: 'iNeed',
        description: 'Payment for iNeed ' + $scope.bid.user.first_name  +' '+ $scope.bid.user.last_name + ' against ' + $scope.con.service.name,
        amount: $scope.bid.total * 100
      });
      $scope.payModal.hide();
      e.preventDefault();
    };

    // Close Checkout on page navigation:
    $scope.$on('$destroy', function() {
      handler.close();
    });
})
.controller('chatCtrl', function($scope, ChatService) {
  $scope.chats = {};
  ChatService.conversation().then(function (res) {
    $scope.chats = res.data;
  });

  $scope.chats = [
    {
      id : 1,
      category : "Website",
      name : "Tech Solutions",
      text : "Php site I can do for 1500€, depending on the features, but ......",
      count : 3,
      provides : 5
    },
    {
      id : 2,
      category : "Plumber",
      name : "Web 2.0",
      text : "I can be there in the next hour, so just let me know if you.....",
      count : 3,
      provides : 4
    },
    {
      id : 3,
      category : "Electrician",
      name : "Mega Tech",
      text : "It depends on how many LED’s you want to insert, but ....",
      count : 10,
      provides : 7
    },
    {
      id : 4,
      category : "Personal Trainer",
      name : "Websoft",
      text : "I focus mainly on Muscle building programmes, so...",
      count : 10,
      provides : 6
    },
    {
      id : 5,
      category : "Baby Sitter",
      name : "Micros",
      text : "I offer you a better service, for only 8,50€ an hour and...",
      count : 5,
      provides : 10
    },
    {
      id : 6,
      category : "Gardener",
      name : "Super Web",
      text : "Tomorrow 9 am, I do the entire garden, and will finish it quicker than....",
      count : 5,
      provides : 5
    },
    {
      id : 7,
      category : "Electrician",
      name : "Electrons",
      text : "It depends on how many LED’s you want to insert, but ....",
      count : 10,
      provides : 4
    },
    {
      id : 8,
      category : "Personal Trainer",
      name : "Syrusgold",
      text : "I focus mainly on Muscle building programmes, so...",
      count : 10,
      provides : 6
    },
    {
      id : 9,
      category : "Baby Sitter",
      name : "Webprops",
      text : "I offer you a better service, for only 8,50€ an hour and...",
      count : 5,
      provides : 10
    },
    {
      id : 10,
      category : "Gardener",
      name : "High Tech",
      text : "Tomorrow 9 am, I do the entire garden, and will finish it quicker than....",
      count : 5,
      provides : 5
    }
  ];
})
   
.controller('favoritesCtrl', function($scope) {
  $scope.favs = [
  {
    img  : 'https://randomuser.me/api/portraits/men/37.jpg',
    name : 'Info Tech',
    text : 'Professional digital website and App development located in.....',
    contact : 'www.info-tech.com'
  },
  {
    img  : 'https://randomuser.me/api/portraits/men/17.jpg',
    name : 'Olga cleaner',
    text : 'Professional cleaning in your home, I clean and do the wash...',
    contact : '0049-167876453'
  },
  {
    img  : 'https://randomuser.me/api/portraits/women/30.jpg',
    name : 'Electric 123',
    text : 'We provide all electrical work, from lamp installing to support...',
    contact : 'www.electric123.de'
  },
  {
    img  : 'https://randomuser.me/api/portraits/women/10.jpg',
    name : 'Info Tech',
    text : 'Professional digital website and App development located in.....',
    contact : 'www.info-tech.com'
  },
  {
    img  : 'https://randomuser.me/api/portraits/women/1.jpg',
    name : 'Patrick Personal Trainer',
    text : 'I will get you into shape, easy and effectively. Write me...',
    contact : '0049-175463849'
  }
  ];
})
      

   
.controller('settingsCtrl', function($scope, Auth, $localStorage) {
  // Logout 
  $scope.logout = Auth.logout;

  // Me object
  $scope.me = $localStorage.me;
})

.controller('chatPrivateCtrl', function($scope, $ionicActionSheet, $timeout, $ionicScrollDelegate) {
  var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
      
      // mock acquiring data via $stateParams
      $scope.toUser = {
        _id: '534b8e5aaa5e7afc1b23e69b',
        pic: 'http://ionicframework.com/img/docs/venkman.jpg',
        username: 'Info Tech'
      };

      // this could be on $rootScope rather than in $stateParams
      $scope.user = {
        _id: '534b8fb2aa5e7afc1b23e69c',
        pic: 'http://ionicframework.com/img/docs/mcfly.jpg',
        username: 'Me'
      };
      
      $scope.onMessageHold = function(e, itemIndex, message) {
        console.log('onMessageHold');
        console.log('message: ' + JSON.stringify(message, null, 2));
        $ionicActionSheet.show({
          buttons: [{
            text: 'Copy Text'
          }, {
            text: 'Delete Message'
          }],
          buttonClicked: function(index) {
            switch (index) {
              case 0: // Copy Text
                //cordova.plugins.clipboard.copy(message.text);

                break;
              case 1: // Delete
                // no server side secrets here :~)
                $scope.messages.splice(itemIndex, 1);
                $timeout(function() {
                  viewScroll.resize();
                }, 0);

                break;
            }
            
            return true;
          }
        });
      };
      
      $scope.messages = [
     {
        "_id":"546a5843fd4c5d581efa263a",
        "text":"Hi, I want to ask you about the price you have suggested.",
        "userId":"534b8fb2aa5e7afc1b23e69c",
        "date":"2014-11-17T20:19:15.289Z",
        "read":true,
        "readDate":"2014-12-01T06:27:38.328Z"
     }
  ];
})

.controller('paymentCtrl', function($scope, $stateParams, inConfig, stripe) {
  $scope.payment = $stateParams;
  console.log($stateParams);
  $scope.stripe_key = inConfig.STRIPE_KEY;


  // $scope.charge = function () {
  //   return stripe.card.createToken($scope.payment.card)
  //     .then(function (response) {
  //       console.log('token created for card ending in ', response.card.last4);
  //       var payment = angular.copy($scope.payment);
  //       payment.card = void 0;
  //       payment.token = response.id;
  //       return $http.post('https://yourserver.com/payments', payment);
  //     })
  //     .then(function (payment) {
  //       console.log('successfully submitted payment for $', payment.amount);
  //     })
  //     .catch(function (err) {
  //       if (err.type && /^Stripe/.test(err.type)) {
  //         console.log('Stripe error: ', err.message);
  //       }
  //       else {
  //         console.log('Other error occurred, possibly with your API', err.message);
  //       }
  //     });
  // };
})

.controller('providersCtrl', function($scope) {

})

.controller('providerDetailsCtrl', function($scope) {

})
 