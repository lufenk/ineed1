angular.module('app.directives', [])
// back button 
.directive('inBack', function($ionicConfig, $ionicHistory, $state){
  return {
    restrict : 'E',
    scope : {to : '@'},
    template : '<button ng-click="$ionicGoBack()" class="button back-button buttons button-clear header-item"><i class="icon {{icon}}"></i> </button>',
    link : function ( scope, elem, attr ) {
      scope.$ionicGoBack = function() {
        if(scope.to) {
          $state.go($ionicHistory.backView() ? $ionicHistory.backView().stateName : scope.to);
        } else {
          if(! $ionicHistory.backView()) $state.go('app.dashboard');
          $ionicHistory.goBack();
        }
      };

      var defaultIcon = $ionicConfig.backButton.icon();
      if (defaultIcon && defaultIcon !== 'none') {
        scope.icon = defaultIcon;
      }
    }
  };
})

.directive('hires', function() {
  return {
    restrict: 'A',
    scope: { hires: '@' },
    link: function(scope, element, attrs) {

      element.attr('src', 'img/avatar-placeholder.svg');
      
      element.one('load', function(e) {
        // console.log(e);
        element.attr('src', scope.hires);
      });

      element.one('error', function(e) {
        element.attr('src', 'img/avatar-placeholder.svg');
        // console.log(e);
      });
    }
  };
})

.directive('blankDirective', [function(){

}]);

