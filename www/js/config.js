angular.module('app')

.config(function($ionicConfigProvider, inConfig, $authProvider, cfpLoadingBarProvider, $localStorageProvider) {
    // Back Button
    $ionicConfigProvider.backButton.previousTitleText(false).text('');

    // Title align
    $ionicConfigProvider.navBar.alignTitle('center');

    //Default spinner 
    $ionicConfigProvider.spinner.icon('ripple');

    // Native scroll
    $ionicConfigProvider.scrolling.jsScrolling(false);
    
    // Loading bar
	  cfpLoadingBarProvider.includeSpinner = false;

    // ngStorage
    $localStorageProvider.setKeyPrefix('in_');
    
    // Auth Providers
    var commonConfig = {
      popupOptions: {
        location: 'no',
        toolbar: 'yes',
        width: window.screen.width,
        height: window.screen.height
      }
    };

    if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
      commonConfig.redirectUri = 'http://localhost/';
    }

    $authProvider.baseUrl = inConfig.API_URL;
    $authProvider.tokenPrefix = inConfig.PREFIX;


    $authProvider.facebook(angular.extend({}, commonConfig, {
      clientId: '603122136500203',
      url: 'http://localhost:3000/auth/facebook'
    }));

    $authProvider.twitter(angular.extend({}, commonConfig, {
      url: 'http://localhost:3000/auth/twitter'
    }));

    $authProvider.google(angular.extend({}, commonConfig, {
      clientId: '631036554609-v5hm2amv4pvico3asfi97f54sc51ji4o.apps.googleusercontent.com',
      url: 'http://localhost:3000/auth/google'
    }));

    // Error Interceptor for API
    // $httpProvider.interceptors.push('errorInterceptor');
});

