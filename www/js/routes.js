angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/app/query');

  $stateProvider

  .state('welcome', {
    url: '/welcome',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('emailLogin', {
    url: '/email-login',
    templateUrl: 'templates/email-login.html',
    controller: 'loginCtrl'
  })

  .state('settings', {
    url: '/settings',
    templateUrl: 'templates/settings.html',
    controller: 'settingsCtrl'
  })

  // .state('pay', {
  //   url: '/pay/:id/:amount',
  //   templateUrl: 'templates/payment.html',
  //   controller: 'paymentCtrl'
  // })
  
  .state('app', {
    url: '/app',
    templateUrl: 'templates/tabsController.html',
    controller : 'appCtrl',
    resolve: {
        loginRequired: function(Auth){
          return Auth.loginRequired();
        },
        bootData : function(DataService, Auth) {
            if(! Auth.isAuthenticated() )  return false;

            return DataService.boot();
        }
    },
    abstract:true
  })

  .state('app.query', {
    url: '/query',
    views: {
      'tabQuery': {
        templateUrl: 'templates/query.html',
        controller: 'queryCtrl'
      }
    }
  })

  .state('app.chats', {
    url: '/chat',
    views: {
      'tabChat': {
        templateUrl: 'templates/chat.html',
        controller: 'chatCtrl'
      }
    }
  })
  
  .state('app.conversation', {
    url: '/conversation/:id',
    views: {
      'tabChat': {
        resolve: {
          conversationData : function(ChatService, $stateParams) {
              return ChatService.findConversation($stateParams.id);
          }
        },
        templateUrl: 'templates/conversation.html',
        controller: 'conversationCtrl'
      }
    }
  })

  .state('app.chatPrivate', {
    url: '/private-chat/:id',
    views: {
      'tabChat': {
        templateUrl: 'templates/chatPrivate.html',
        controller: 'chatPrivateCtrl'
      }
    }
  })

  .state('app.providers', {
    url: '/providers/:id',
    views: {
      'tabChat': {
        templateUrl: 'templates/providers.html',
        controller: 'chatCtrl'
      }
    }
  })
  
  .state('app.providerDetails', {
    url: '/provider/:id',
    views: {
      'tabChat': {
        templateUrl: 'templates/providerDetails.html',
        controller: 'providerDetailsCtrl'
      }
    }
  })

  .state('app.favorites', {
    url: '/favorites',
    views: {
      'tabFav': {
        templateUrl: 'templates/favorites.html',
        controller: 'favoritesCtrl'
      }
    }
  });

});