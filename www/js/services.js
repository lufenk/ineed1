angular.module('app.services', [])


// Data Service, wrapper around $http
.factory('DataService', function($http, inConfig, $localStorage){
    
        var baseUrl = inConfig.API_URL;
        var bootData;

        var service = {
            head: head,
            get: get,
            post : post,
            put : put,
            patch : patch,
            delete : del,
            jsonp : jsonp,
            getAbsolute : getAbsolute,
            postAbsolute: postAbsolute,
            boot : boot,
            getBootData : getBootData
        };

        return service;
    
        /////////////////
    
        function _get (url, config) {
            return $http.get(url, config);
        }

        function get(url, config){
            url = url || "";
            return _get(baseUrl + url, config);
        }

        function getAbsolute(url, config) {
            return $http.get(url, config);
        }

        function postAbsolute(url, data, config) {
            return $http.post(url, data, config);
        }

        function head(url, config){
            url = url || "";
            return $http.head(baseUrl + url, config);
        }

        function post(url, data, config){
            url = url || "";
            return $http.post(baseUrl + url, data, config);
        }

        function put(url, data, config){
            url = url || "";
            return $http.put(baseUrl + url, data, config);
        }

        function patch(url, data, config){
            url = url || "";
            return $http.patch(baseUrl + url, data, config);
        }

        function jsonp(url, config){
            url = url || "";
            return $http.jsonp(baseUrl + url, config);
        }

        function del(url, config){
            url = url || "";
            return $http.delete(baseUrl + url, config);
        }
    
        function boot() {
            return get('boot').then(function(res){
                bootData = res.data;
                inConfig.config = bootData.config;
                // Save user into storage
                $localStorage.me = bootData.me;
                return res;
            });
        }
    
        function getBootData() {
            
            if(bootData) {
                return bootData;
            }
            
            return boot();
        }
})

// Auth Service
.factory('Auth', function($auth, DataService, $q, $location, Util, $localStorage, $ionicHistory, $state){

        var auth = {
            login : $auth.login,
            signup : $auth.signup,
            logout : logout,
            setToken : $auth.setToken,
            getToken : $auth.getToken,
            authenticate : $auth.authenticate,
            isAuthenticated : $auth.isAuthenticated,
            loginRequired : loginRequired,
            recover : recover,
            setupProfile : setupProfile,
            user  : user
        };

        return auth;

        /////////////////

        function recover(email, config) {
            return DataService.post('user/forgot', { email : email }, config);
        }

        function logout() {
            Util.confirm({
                title : 'Logout',
                template : 'Are you sure want to logout?'
            }, function  (ans) {
                if(ans) {
                    console.info('Loging out');
                    // Logout and clear Data
                    $ionicHistory.nextViewOptions({
                          historyRoot: true,
                          disableBack: true,
                    });
                
                    $ionicHistory.clearCache().then(function() {
                        // Logout
                        $auth.logout();

                        //now clear history
                        $ionicHistory.clearHistory();

                        // Delete all cached data from localStorage
                        $localStorage.$reset();

                        // Redirect to login
                        $state.go('welcome', {}, { reload : true });
                    });
                }
            });
        }
    
        function setupProfile(res) {            
            // store user in localStorage
        }
    
        function user() {
            return $localStorage.me;
        }

        function loginRequired() {
            var deferred = $q.defer();
            
            if ($auth.isAuthenticated()) {
                deferred.resolve();
            } else {
                $location.path('/welcome');
            }
            
            return deferred.promise;
        }
})

// Point Service
.factory('LocationService', function(Util, $q, cfpLoadingBar){
    
    var service = {
        getCurrentLocation : getCurrentLocation
    };
    
    return service;

    /////////////////
    
    function getCurrentLocation() {
        
        var posOptions = {
            timeout: 10000, 
            enableHighAccuracy: true
        };

        var deferred = $q.defer();
        
        // Start
        cfpLoadingBar.start();
        cfpLoadingBar.inc();

        // Call for location
        navigator.geolocation
            .getCurrentPosition(function (position) {
                deferred.resolve(position.coords);
                cfpLoadingBar.complete();
            }, function (err) {
                deferred.reject(err);
                cfpLoadingBar.complete();
                Util.popup('Error', err.message);
            }, posOptions);

        return deferred.promise;
    }

})

// Util
.factory('Util', function ($ionicPopup) {
    var utils = {
        showValidationError : showValidationError,
        popup : popup,
        confirm : confirm,
        splitName : splitName
    };

    return utils;

    /////////////////

    function showValidationError (res) {
        if(res.status === 422) {
            var msg = "";
            if(res.data && res.data.errors) {
                // Print errors
                for (var error in res.data.errors) {
                    msg += '<span class="assertive">&diams;</span> ' + res.data.errors[error][0] + "<br>";
                }

                if(msg) {
                  // show the error
                  popup('Validation Error', msg);
                }
            } else {
                console.error('Validation error not formated currectrly');    
            }
        } else {
            if(res.data && res.data.message) {
                popup('Whoops!', res.data.message);
            } else {
                // show general error msg
                popup('Whoops!', 'Sorry! Unable to proccess your request, try again');
            }
        }
    }

    function splitName (fullName) {
        var result = {};
        if (fullName) {
            fullName = fullName.trim();

            var nameArr = fullName.split(' ');
            // check for last name
            if(nameArr.length > 1) {
                result.lastName = nameArr.pop().trim();
                result.firstName = nameArr.join(' ').trim();    
            } else {
                result.firstName = fullName;
            }            
        }
        return result;
    }

    function popup (title, content) {
        $ionicPopup.alert({ title: title, content: content });
    }

    function confirm ( options, callback ) {
        // A confirm dialog
         var confirmPopup = $ionicPopup.confirm( options );

         confirmPopup.then(callback || function( res ) {
           console.log('confirmation was ' + res);
         });
    }
})


// Chat Service
.factory('ChatService', function(DataService){
    // hold the data
    var data = {};

    var service = {
        send: send,
        deleteMessage : deleteMessage,
        conversation : conversation,
        archiveConversation : archiveConversation,
        findConversation : findConversation,
        getData : getData
    };
    
    return service;

    /////////////////
    
    function send(data) {
        return DataService.post( 'messages', data, { ignoreLoadingBar: true });
    }

    function deleteMessage(id) {
        return DataService.delete( 'messages/' + id);
    }

    function conversation () {
        return DataService.get( 'conversations' );
    }

    function findConversation ( conversation_id ) {
        data.conversation = DataService.get( 'conversations/' + conversation_id );
        return data.conversation;
    }

    function archiveConversation (conversation_id) {
        return DataService.post( 'conversations/archive/' + conversation_id, {} );
    }

    function getData () {
        return data;
    }

})

// Error Interceptor
.factory('errorInterceptor', function(){
    
    var errorInterceptor = {
        responseError: responseError
    };
    
    return errorInterceptor;

    /////////////////
    
    function responseError(res) {
     var msg;
     // Handle 401
     if(res.status === 401) {
         msg = res.message ? res.message : 'Unauthorized to access';
         console.info(msg);
     }

     return res;
    }

})
// services
.factory('MockService', ['$http', '$q',
  function($http, $q) {
    var me = {};

    me.getUserMessages = function(d) {
      
      var endpoint =
        'http://www.mocky.io/v2/547cf341501c337f0c9a63fd?callback=JSON_CALLBACK';
      return $http.jsonp(endpoint).then(function(response) {
        return response.data;
      }, function(err) {
        console.log('get user messages error, err: ' + JSON.stringify(
          err, null, 2));
      });
      
      // var deferred = $q.defer();
      
      //    setTimeout(function() {
      //   deferred.resolve(getMockMessages());
      //   }, 1500);
      
      // return deferred.promise;
    };

    me.getMockMessage = function() {
      return {
        userId: '534b8e5aaa5e7afc1b23e69b',
        date: new Date(),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
      };
    };

    return me;
  }
]);
